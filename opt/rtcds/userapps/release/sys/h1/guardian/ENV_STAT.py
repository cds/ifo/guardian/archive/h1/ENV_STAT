"""Node used to determine the environment conditions and then display
that as the state.

Should be one main decorator that will check and decide what state we 
should go into.

No edges should be necessary, all just jumpin.

**Update: 2019-12-13**: Lets start out by just doing EQs. 
States:
  CALM
    Nominal/start state
  SEISMON_ALERT
    If we didn't advance in 10min after event, jump back.
  EARTHQUAKE
    We currently have ground motion, should look at peakmon to get here
"""
from guardian import GuardState, GuardStateDecorator
import gpstime
import math

nominal = 'CALM'
#############################################
# Decorator and supporting functions/constants

peakmon_thresh = 1000
peakmon_high = 1000

y3_raw = lambda j: 0.75 * j + 0

def seismon_active():
    """Return True/False if there is an active seismon alert.
    By active I mean, in at least the green band.
    """
    for i in range(5):
        if math.log(ezca['SEI-SEISMON_EQ_LHO_DISTANCE_M_{}'.format(i)] / 1000, 10)\
           < y3_raw(ezca['SEI-SEISMON_EQ_MAGNITUDE_MMS_{}'.format(i)]) and \
           ezca['SEI-SEISMON_LHO_R35_ARRIVALTIME_TOTAL_SECS_{}'.format(i)] > 0:
            return True
    # If we made it this far, theres no active seismon
    return False


def peakmon_active():
    """See if there is some current ground motion on peakmon."""
    if ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'] > peakmon_thresh:
        return True
    else:
        return False


def env_check():
    # Order will matter here, we dont want to get stuck in loop
    # between states.
    if ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'] > 500 and seismon_active():
        return 'EARTHQUAKE'
    else:
        return 'WINDY'
    


class check_env_state(GuardStateDecorator):
    """Main decorator

    """
    def pre_exec(self):
        return env_check()
#############################################
# States

def gen_idle_state(index, statename):
    class IDLE(GuardState):
        #@check_env_state
        def run(self):
            state = env_check()
            if state != str(statename):
                return state
            else:
                return True
    IDLE.index = index
    return IDLE


WINDY = gen_idle_state(50, 'WINDY')



class CALM(GuardState):
    index = 10
    def main(self):
        return False
    def run(self):
        if seismon_active():
            log('Seismon active')
            return 'SEISMON_ALERT'
        elif peakmon_active():
            if ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'] > peakmon_high:
                log('Peakmon high, no seismon alert, jumping to EARTHQUAKE')
                return 'EARTHQUAKE'
        else:
            return True


class SEISMON_ALERT(GuardState):
    """Wait for EQ. After seismon alert expires, wait another
    10min before going back.
    """
    index = 20
    def main(self):
        self.timer['wait_for_eq'] = 0
        self.seismon_expired = False
        
    def run(self):
        if peakmon_active():
            log('Peakmon active, moving to EARTHQUAKE')
            return 'EARTHQUAKE'
        elif not seismon_active() and not self.seismon_expired:
            self.seismon_expired = True
            log('Seismon expired, waiting for 10min.')
            self.timer['wait_for_eq'] = 600
        elif not seismon_active() and self.seismon_expired and self.timer['wait_for_eq']:
            log('EQ didnt show')
            return 'CALM'
        

class EARTHQUAKE(GuardState):
    # FIXME: How long are we staying in this state? When can we move out?
    # If we move out too soon, we risk having another wave hit us.

    def main(self):
        self.timer['calm'] = 0
        self.calm = False

    def run(self):
        if not peakmon_active() and not self.calm and self.timer['calm']:
            self.calm = True
            # 10min? Sure why not
            self.timer['calm'] = 600
        elif peakmon_active() and self.calm:
            # reset the calm
            self.calm = False
        elif not peakmon_active() and self.calm and self.timer['calm']:
            log('Low ground for 10min, going back to CALM')
            return 'CALM'
        

'''
class EARTHQUAKE(GuardState):

    def main(self):
        self.state_start = gpstime.gpsnow()
        self.calm = False

    def run(self):
        if ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'] > 500 and seismon_active():
            notify('Incoming earthquake')
        elif ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'] > 500 and not seismon_active():
            notify('Waiting for the earth to settle')
        # The first time that we are calm
        elif ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'] <= 500 \
             and not seismon_active() and self.calm == False:
            log('Calm and no EQs predicted, starting timer to switch')
            self.calm = True
            # Wait for some period before switching out of this state
            self.timer['calm'] = 300
        # Calm and timer up
        elif ezca['ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON'] <= 500 \
             and not seismon_active() \
             and self.calm == True \
             and self.timer['calm']:
            log('All settled')
            # Maybe do this?
            state = env_check()
            if state != 'EARTHQUAKE':
                return state
            else:
                return True
'''
